import java.util.Scanner;

public class HW_2_3_3 {
    public static double pow(double x, int y) {
        double pow = x;
        for (int i = 1; i < y; i++) {
            pow *= x;
        }
        if (y == 0) {
            pow = 1;
        }
        return pow;
    }

    public static double rectangleMethod(double a, double b, double n, int y) {
        double h = (b - a) / n;
        double result = 0;
        for (int k = 1; k < n; k++) {
            double x = (a + h * k) - h / 2;
            double currentRectangle = pow(x, y);
            result = result + currentRectangle;
        }
        return result * h;
    }

    public static double simpsonMethod(double a, double b, double n, int y) {
        double h = (b - a) / n;
        double result = 0;
        for (int k = 1; k < n - 1; k += 2) {
            double x1 = a + h * (k - 1), x2 = a + h * k, x3 = a + h * (k + 1);
            double currentRectangle = pow(x1, y) + 4 * pow(x2, y) + pow(x3, y);
            result = result + currentRectangle;
        }
        return result * h / 3;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 'a' segment [a,b]:");
        double a = scanner.nextDouble();
        System.out.println("Enter 'b' segment [a,b]:");
        double b = scanner.nextDouble();
        double ns[] = {100, 1000, 10000, 100000, 1000000};
        double ys[] = {2, 3, 4, 5, 6, 7};

        System.out.println("_______________________________________________________________");
        System.out.println("|    Method name    | Quantity iterations | Calculation result|");
        System.out.println("_______________________________________________________________");

        for (int i = 0; i <= ys.length - 1; i++) {
            for (int j = 0; j <= ns.length - 1; j++) {
                double rectangleResult = rectangleMethod(a, b, ns[j], (int) ys[i]);
                System.out.println("| Rectangle Method  | " + ns[j] + " - x^" + ys[i] + " | " + rectangleResult + "  |");
                System.out.println("_______________________________________________________________");
            }
        }

        for (int i = 0; i <= ys.length - 1; i++) {
            for (int j = 0; j <= ns.length - 1; j++) {
                double rectangleResult = simpsonMethod(a, b, ns[j], (int) ys[i]);
                System.out.println("| Simpson Method  | " + ns[j] + " - x^" + ys[i] + " | " + rectangleResult + "  |");
                System.out.println("_______________________________________________________________");
            }
        }
    }
}
