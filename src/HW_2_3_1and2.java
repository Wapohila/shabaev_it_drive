import java.util.Random;
import java.util.Scanner;

public class HW_2_3_1and2 {
    public static void swap(int a[], int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void selectionSort(int a[]) {
        for (int i = 0; i < a.length; i++) {
            int min = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < a[min])
                    min = j;
            }
            swap(a, min, i);
        }
    }

    public static void search(int a[], int numberForSearch) {
        int left = 0, right = a.length - 1, middle;
        boolean find = false;

        while (left <= right) {
            middle = (right + left) / 2;
            if (a[middle] < numberForSearch) {
                left = middle + 1;
            } else if (a[middle] > numberForSearch) {
                right = middle - 1;
            } else {
                System.out.println("Exists");
                find = true;
                break;
            }
        }
        if (find == false) {
            System.out.println("Not exists");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Enter the size of the array:");
        int length = scanner.nextInt();
        int array[] = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(length);
        }
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.print(array[array.length - 1]);

        System.out.println(" ");
        System.out.println(" ");
        selectionSort(array);

        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[array.length - 1]);

        System.out.print("Enter the search number:");
        int numberForSearch = scanner.nextInt();

        search(array, numberForSearch);
    }
}
