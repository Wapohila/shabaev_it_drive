import java.util.Scanner;

public class HW_2_4_1 {
    public static int fib(int i, int n, int x1, int x2, int sum) {
        if (i == 2) {
            x2 = 1;
            x1 = x2;
        }
        sum = x1 + x2;
        x2 = x1;
        x1 = sum;
        if (i == n-1) {
            return sum;
        }
        int result = fib(i + 1, n, x1, x2, sum);
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x1 = 0;
        int x2 = 0;
        int sum = 0;
        int i = 2;
        int result = fib(i, n, x1, x2, sum);
        System.out.println(result);
    }
}
